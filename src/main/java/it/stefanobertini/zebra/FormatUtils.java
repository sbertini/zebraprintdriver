package it.stefanobertini.zebra;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class FormatUtils {

    public static String format(double text) {
        int intText;
        intText = (int) text;
        if ((double) intText == text) {
            return "" + intText;
        } else {
            DecimalFormatSymbols symbols = new DecimalFormatSymbols();
            symbols.setDecimalSeparator('.');
            DecimalFormat format = new DecimalFormat("0.#######", symbols);
            return format.format(text);
        }
    }

    private final static char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();

    public static String bytesToHex(int[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }
}
