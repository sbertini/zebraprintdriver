package it.stefanobertini.zebra;

import java.util.List;

public class Validator {

    public static void signalError(String message) {
        throw new ValidationException(message);
    }

    public static void isMoreThan(String parameterName, double actualValue, double expectedValue) {
        if (!(actualValue > expectedValue)) {
            signalError(buildMessage(parameterName, actualValue, "> " + FormatUtils.format(expectedValue)));
        }
    }

    public static void isMoreThanOrEqualTo(String parameterName, double actualValue, double expectedValue) {
        if (!(actualValue >= expectedValue)) {
            signalError(buildMessage(parameterName, actualValue, ">= " + FormatUtils.format(expectedValue)));
        }
    }

    public static void isLessThan(String parameterName, double actualValue, double expectedValue) {
        if (!(actualValue > expectedValue)) {
            signalError(buildMessage(parameterName, actualValue, "< " + FormatUtils.format(expectedValue)));
        }
    }

    public static void isLessThanOrEqualTo(String parameterName, double actualValue, double expectedValue) {
        if (!(actualValue >= expectedValue)) {
            signalError(buildMessage(parameterName, actualValue, "<= " + FormatUtils.format(expectedValue)));
        }
    }

    public static void isBetween(String parameterName, double actualValue, double minExpectedValue, double maxExpectedValue) {
        if (!(actualValue >= minExpectedValue && actualValue <= maxExpectedValue)) {
            signalError(buildMessage(parameterName, actualValue,
                    "between " + FormatUtils.format(minExpectedValue) + " and " + FormatUtils.format(maxExpectedValue)));
        }
    }

    public static void isRequired(String parameterName, String value) {
        if (value == null || value.length() == 0) {
            signalError("Parameter " + parameterName + " is mandatory.");
        }
    }

    public static void isRequired(String parameterName, Object value) {
        if (value == null) {
            signalError("Parameter " + parameterName + " is mandatory.");
        }
    }

    @SuppressWarnings("rawtypes")
    public static void isNotEmpty(String parameterName, List object) {
        if (object == null || object.size() == 0) {
            signalError("Parameter " + parameterName + " is mandatory.");
        }
    }

    private static String buildMessage(String parameterName, double actualValue, String shouldBe) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("Parameter ");
        buffer.append(parameterName);
        buffer.append(" is ");
        buffer.append(FormatUtils.format(actualValue));
        buffer.append(". Should be ");
        buffer.append(shouldBe);
        return buffer.toString();
    }
}
