package it.stefanobertini.zebra.beans;

public class Font {

    private String font;
    private int size;

    public Font(String font, int size) {
        super();
        this.font = font;
        this.size = size;
    }

    public String getFont() {
        return font;
    }

    public int getSize() {
        return size;
    }

    public String getCommandLine() {
        return font + " " + size;
    }

    @Override
    public String toString() {
        return "Font [font=" + font + ", size=" + size + "]";
    }

}