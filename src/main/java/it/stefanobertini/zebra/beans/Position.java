package it.stefanobertini.zebra.beans;

import it.stefanobertini.zebra.FormatUtils;

public class Position {

    private double x = 0;
    private double y = 0;

    public Position() {
    }

    public Position(double x, double y) {
        super();
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public String getCommandLine() {
        return FormatUtils.format(x) + " " + FormatUtils.format(y);
    }

    @Override
    public String toString() {
        return "Position [x=" + FormatUtils.format(x) + ", y=" + FormatUtils.format(y) + "]";
    }

}