package it.stefanobertini.zebra.beans;

import it.stefanobertini.zebra.enums.QRCodeErrorCorrectionLevel;
import it.stefanobertini.zebra.enums.QRCodeMaskNumber;

public class QRCodeAutomaticData extends QRCodeAbstractData implements QRCodeDataInterface {

    private String data = "";

    public QRCodeAutomaticData(QRCodeErrorCorrectionLevel errorCorrectionLevel, QRCodeMaskNumber maskNumber, String data) {
        super(errorCorrectionLevel, maskNumber, "A");
        this.data = data;
    }

    public String getCommandLine() {
        StringBuffer buffer = new StringBuffer();

        buffer.append(super.getCommandLine());
        buffer.append(data);
        buffer.append("\r\n");

        return buffer.toString();
    }

}
