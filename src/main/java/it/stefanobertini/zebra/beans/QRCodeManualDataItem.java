package it.stefanobertini.zebra.beans;

import it.stefanobertini.zebra.enums.QRCodeCharacterMode;

public class QRCodeManualDataItem {

    private QRCodeCharacterMode characterMode;
    private String data;

    public QRCodeManualDataItem(QRCodeCharacterMode characterMode, String data) {
        super();
        this.characterMode = characterMode;
        this.data = data;
    }

    /**
     * @return the characterMode
     */
    public QRCodeCharacterMode getCharacterMode() {
        return characterMode;
    }

    /**
     * @param characterMode
     *            the characterMode to set
     */
    public void setCharacterMode(QRCodeCharacterMode characterMode) {
        this.characterMode = characterMode;
    }

    /**
     * @return the data
     */
    public String getData() {
        return data;
    }

    /**
     * @param data
     *            the data to set
     */
    public void setData(String data) {
        this.data = data;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "QRCodeManualDataItem [characterMode=" + characterMode + ", data=" + data + "]";
    }

}
