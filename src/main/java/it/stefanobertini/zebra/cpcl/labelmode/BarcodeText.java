package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.Validator;
import it.stefanobertini.zebra.beans.Font;

public class BarcodeText extends AbstractCommand implements LabelModeCommandInterface {

    private Font font = null;
    private double offset;

    public BarcodeText(Font font, double offset) {
        super();
        this.font = font;
        this.offset = offset;
    }

    public String getCommand() {
        return "BARCODE-TEXT";
    }

    @Override
    public void getCommandLineInternal() {

        appendText(getCommand());
        appendText(" ");
        appendText(font.getCommandLine());
        appendText(" ");
        appendText(offset);

        endLine();
    }

    @Override
    public void validate() {
        Validator.isRequired("font", font);
        Validator.isMoreThanOrEqualTo("offset", offset, 0);
    }

    public Font getFont() {
        return font;
    }

    public void setFont(Font font) {
        this.font = font;
    }

    public double getOffset() {
        return offset;
    }

    public void setOffset(double offset) {
        this.offset = offset;
    }

    @Override
    public String toString() {
        return "BarcodeText [font=" + font + ", offset=" + offset + "]";
    }
}
