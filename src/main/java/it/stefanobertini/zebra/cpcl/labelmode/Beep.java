package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractIntegerParameterCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.Validator;

public class Beep extends AbstractIntegerParameterCommand implements LabelModeCommandInterface {

    public Beep() {
        this(0);
    }

    public Beep(int length) {
        setParameter(length);
    }

    public String getCommand() {
        return "BEEP";
    }

    @Override
    public void validate() {
        Validator.isMoreThanOrEqualTo("length", getParameter(), 0);
    }

    public int getLength() {
        return getParameter();
    }

    public void setLength(int length) {
        setParameter(length);
    }

    @Override
    public String toString() {
        return "Beep [length=" + getParameter() + "]";
    }

}
