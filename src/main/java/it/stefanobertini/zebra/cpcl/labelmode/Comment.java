package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractStringParameterCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;

/**
 * 
 * Implements the COMMENT command<br/>
 * <br/>
 * Description:<br>
 * The COMMENT command insert a comment into a command file. <br/>
 * <br/>
 * Example:<br/>
 * 
 * <pre>
 * ; {comment text}
 * </pre>
 * 
 * @author stefano.bertini[at]gmail.com
 * 
 */

public class Comment extends AbstractStringParameterCommand implements LabelModeCommandInterface {

    public Comment() {
    }

    public Comment(String comment) {
        setParameter(comment);
    }

    public String getCommand() {
        return ";";
    }

    @Override
    public void validate() {
    }

    public String getComment() {
        return getParameter();
    }

    public void setComment(String comment) {
        setParameter(comment);
    }

    @Override
    public String toString() {
        return "Comment [comment=" + getParameter() + "]";
    }

}
