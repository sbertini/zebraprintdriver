package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.FormatUtils;
import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.Validator;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.beans.Size;
import it.stefanobertini.zebra.enums.Orientation;

public class CompressedGraphics extends AbstractCommand implements LabelModeCommandInterface {

    private Orientation orientation = Orientation.horizontal;
    private Size size;
    private Position position;
    private String data;

    public CompressedGraphics(Orientation orientation, Size size, Position position, String data) {
        super();
        this.orientation = orientation;
        this.size = size;
        this.position = position;
        this.data = data;
    }

    public CompressedGraphics(Orientation orientation, Size size, Position position, int[] data) {
        super();
        this.orientation = orientation;
        this.size = size;
        this.position = position;
        this.data = FormatUtils.bytesToHex(data);
    }

    public String getCommand() {
        return Orientation.vertical.equals(orientation) ? "VCOMPRESSED-GRAPHICS" : "COMPRESSED-GRAPHICS";
    }

    @Override
    public void getCommandLineInternal() {

        appendText(getCommand());

        appendText(" ");
        appendText(size.getCommandLine());
        appendText(" ");
        appendText(position.getCommandLine());
        appendText(" ");
        appendText(data);

        endLine();
    }

    @Override
    public void validate() {
        Validator.isRequired("orientation", orientation);
        Validator.isRequired("size", size);
        Validator.isRequired("position", position);
        Validator.isRequired("data", data);
    }

    /**
     * @return the orientation
     */
    public Orientation getOrientation() {
        return orientation;
    }

    /**
     * @param orientation
     *            the orientation to set
     */
    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }

    /**
     * @return the size
     */
    public Size getSize() {
        return size;
    }

    /**
     * @param size
     *            the size to set
     */
    public void setSize(Size size) {
        this.size = size;
    }

    /**
     * @return the position
     */
    public Position getPosition() {
        return position;
    }

    /**
     * @param position
     *            the position to set
     */
    public void setPosition(Position position) {
        this.position = position;
    }

    /**
     * @return the data
     */
    public String getData() {
        return data;
    }

    /**
     * @param data
     *            the data to set
     */
    public void setData(String data) {
        this.data = data;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ExpandedGraphics [orientation=" + orientation + ", size=" + size + ", position=" + position + ", data=" + data + "]";
    }

}
