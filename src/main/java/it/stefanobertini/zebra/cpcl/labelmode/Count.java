package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractIntegerParameterCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;

public class Count extends AbstractIntegerParameterCommand implements LabelModeCommandInterface {

    public Count() {
        this(0);
    }

    public Count(int count) {
        setParameter(count);
    }

    public String getCommand() {
        return "COUNT";
    }

    @Override
    public void validate() {
    }

    public int getCount() {
        return getParameter();
    }

    public void setCount(int count) {
        setParameter(count);
    }

    @Override
    public String toString() {
        return "Count [count=" + getParameter() + "]";
    }

}
