package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractStringParameterCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.Validator;
import it.stefanobertini.zebra.enums.CountryCode;

public class Country extends AbstractStringParameterCommand implements LabelModeCommandInterface {

    public Country() {
        this(CountryCode.usa);
    }

    public Country(String countryCode) {
        setParameter(countryCode);
    }

    public Country(CountryCode countryCode) {
        this.setParameter(countryCode.getCode());
    }

    public String getCommand() {
        return "COUNTRY";
    }

    @Override
    public void validate() {
        Validator.isRequired("countryCode", getParameter());
    }

    public String getCountryCode() {
        return getParameter();
    }

    public void setCountryCode(String countryCode) {
        setParameter(countryCode);
    }

    public void setCountryCode(CountryCode countryCode) {
        setParameter(countryCode.getCode());
    }

    @Override
    public String toString() {
        return "Country [countryCode=" + getParameter() + "]";
    }

}
