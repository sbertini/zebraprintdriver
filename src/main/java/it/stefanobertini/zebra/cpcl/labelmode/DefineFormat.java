package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractStringParameterCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.Validator;

public class DefineFormat extends AbstractStringParameterCommand implements LabelModeCommandInterface {

    public DefineFormat() {
    }

    public DefineFormat(String formatName) {
        setParameter(formatName);
    }

    public String getCommand() {
        return "! DEFINE-FORMAT";
    }

    @Override
    public void validate() {
        Validator.isRequired("formatName", getParameter());
    }

    public String getFormatName() {
        return getParameter();
    }

    public void setFormatName(String formatName) {
        setParameter(formatName);
    }

    @Override
    public String toString() {
        return "DefineFormat [formatName=" + getParameter() + "]";
    }

}
