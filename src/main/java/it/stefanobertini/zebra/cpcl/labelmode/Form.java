package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractNoParameterCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;

/**
 * 
 * Implements the FORM command<br/>
 * <br/>
 * Description:<br>
 * The FORM command instructs the printer to feed to top of form after printing. <br/>
 * <br/>
 * Example:<br/>
 * 
 * <pre>
 * FORM
 * </pre>
 * 
 * @author stefano.bertini[at]gmail.com
 * 
 */

public class Form extends AbstractNoParameterCommand implements LabelModeCommandInterface {

    public String getCommand() {
        return "FORM";
    }

    @Override
    public void validate() {
    }

    @Override
    public String toString() {
        return "Form []";
    }
}
