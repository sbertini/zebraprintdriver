package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractNoParameterCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;

/**
 * 
 * Implements the JOURNAL command<br/>
 * <br/>
 * Description:<br>
 * The JOURNAL command disables the automatic media alignment feature. <br/>
 * <br/>
 * Example:<br/>
 * 
 * <pre>
 * JOURNAL
 * </pre>
 * 
 * @author stefano.bertini[at]gmail.com
 * 
 */

public class Journal extends AbstractNoParameterCommand implements LabelModeCommandInterface {

    public String getCommand() {
        return "JOURNAL";
    }

    @Override
    public void validate() {
    }

    @Override
    public String toString() {
        return "Journal []";
    }
}
