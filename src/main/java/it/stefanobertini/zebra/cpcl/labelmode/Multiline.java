package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.Validator;
import it.stefanobertini.zebra.beans.Font;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.enums.TextRotation;

import java.util.ArrayList;
import java.util.List;

public class Multiline extends AbstractCommand implements LabelModeCommandInterface {

    private double height;
    private TextRotation textRotation;
    private Font font;
    private Position position;
    private List<String> textList = new ArrayList<String>();

    public Multiline(double height, TextRotation textRotation, Font font, Position position) {
        this(height, textRotation, font, position, new ArrayList<String>());
    }

    public Multiline(double height, TextRotation textRotation, Font font, Position position, List<String> textList) {
        super();
        this.height = height;
        this.textRotation = textRotation;
        this.font = font;
        this.position = position;
        this.textList = textList;
    }

    public void addText(String text) {
        textList.add(text);
    }

    public String getCommand() {
        return "MULTILINE";
    }

    @Override
    protected void getCommandLineInternal() {
        appendText(getCommand());
        appendText(" ");
        appendText(height);
        endLine();

        appendText(textRotation.getCode());
        appendText(" ");
        appendText(font.getCommandLine());
        appendText(" ");
        appendText(position.getCommandLine());
        endLine();

        for (String text : textList) {
            appendText(text);
            endLine();
        }

        appendText("ENDMULTILINE");
        endLine();
    }

    @Override
    public void validate() {
        Validator.isRequired("textRotation", textRotation);
        Validator.isRequired("font", font);
        Validator.isRequired("position", position);
        Validator.isNotEmpty("textList", textList);
        Validator.isMoreThanOrEqualTo("height", height, 0);
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public TextRotation getTextRotation() {
        return textRotation;
    }

    public void setTextRotation(TextRotation textRotation) {
        this.textRotation = textRotation;
    }

    public Font getFont() {
        return font;
    }

    public void setFont(Font font) {
        this.font = font;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public List<String> getTextList() {
        return textList;
    }

    public void setTextList(List<String> textList) {
        this.textList = textList;
    }

    @Override
    public String toString() {
        return "Multiline [height=" + height + ", textRotation=" + textRotation + ", font=" + font + ", position=" + position + ", textList=" + textList + "]";
    }

}
