package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.Validator;
import it.stefanobertini.zebra.beans.Position;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Pcx extends AbstractCommand implements LabelModeCommandInterface {

    private Position position = new Position(0, 0);
    private String pcxData;

    public Pcx(Position position) {
        super();
        this.position = position;

    }

    public Pcx loadImageFromFile(File file) {
        pcxData = "\r\n" + readFileToString(file);
        return this;
    }

    public Pcx loadImageFromResource(String resource) {
        pcxData = "\r\n" + readFileToString(this.getClass().getClassLoader().getResourceAsStream(resource));
        return this;
    }

    public Pcx loadImageFromPrinter(String printerFile) {
        pcxData = " !<" + printerFile;
        return this;
    }

    public String getCommand() {
        return "PCX";
    }

    @Override
    public byte[] getCommandByteArray() {

        validate();

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        try {
            bos.write(getCommand().getBytes());

            bos.write(" ".getBytes());
            bos.write(position.getCommandLine().getBytes());

            bos.write(pcxData.getBytes());

            bos.write("\r\n".getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return bos.toByteArray();
    }

    @Override
    protected void getCommandLineInternal() {
        // Unused... sending directly the bytes
    }

    @Override
    public void validate() {
        Validator.isRequired("position", position);
        Validator.isRequired("pcxData", pcxData);
    }

    public ByteArrayOutputStream readFileToString(File file) {
        InputStream is = null;

        try {
            is = new FileInputStream(file);
            return readFileToString(is);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    // Ignore
                }
            }
        }
    }

    public ByteArrayOutputStream readFileToString(InputStream is) {
        InputStreamReader isr = null;
        ByteArrayOutputStream bos = null;
        try {
            isr = new InputStreamReader(is);
            bos = new ByteArrayOutputStream();

            byte[] buffer = new byte[2048];
            int n = 0;
            while (-1 != (n = is.read(buffer))) {
                bos.write(buffer, 0, n);
            }

            return bos;
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (isr != null) {
                try {
                    isr.close();
                } catch (IOException e) {
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                }
            }
        }
    }

    /**
     * @return the position
     */
    public Position getPosition() {
        return position;
    }

    /**
     * @param position
     *            the position to set
     */
    public void setPosition(Position position) {
        this.position = position;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Pcx [position=" + position + "]";
    }

}
