package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.Validator;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.beans.QRCodeDataInterface;
import it.stefanobertini.zebra.enums.Orientation;
import it.stefanobertini.zebra.enums.QRCodeModelType;

public class QRCode extends AbstractCommand implements LabelModeCommandInterface {
    private Orientation orientation = Orientation.horizontal;
    private Position position;
    private QRCodeModelType qrCodeModelType = QRCodeModelType.model2;
    private double unitWidth = 6;

    private QRCodeDataInterface data;

    public QRCode(Position position) {
        this(Orientation.horizontal, position, QRCodeModelType.model2, 6, null);
    }

    public QRCode(Position position, QRCodeDataInterface data) {
        this(Orientation.horizontal, position, QRCodeModelType.model2, 6, data);
    }

    public QRCode(Orientation orientation, Position position, QRCodeModelType qrCodeModelType, double unitWidth) {
        this(orientation, position, qrCodeModelType, unitWidth, null);
    }

    public QRCode(Orientation orientation, Position position, QRCodeModelType qrCodeModelType, double unitWidth, QRCodeDataInterface data) {
        super();
        this.orientation = orientation;
        this.position = position;
        this.qrCodeModelType = qrCodeModelType;
        this.unitWidth = unitWidth;
        this.data = data;
    }

    public String getCommand() {
        return (Orientation.vertical.equals(orientation) ? "VBARCODE" : "BARCODE") + " QR";
    }

    @Override
    protected void getCommandLineInternal() {
        appendText(getCommand());
        appendText(" ");
        appendText(position.getCommandLine());
        appendText(" ");
        appendText("M");
        appendText(" ");
        appendText(qrCodeModelType.getCode());
        appendText(" ");
        appendText("U");
        appendText(" ");
        appendText(unitWidth);
        endLine();

        appendText(data.getCommandLine());

        appendText("ENDQR");
        endLine();
    }

    @Override
    public void validate() {
        Validator.isRequired("orientation", orientation);
        Validator.isRequired("position", position);
        Validator.isRequired("qrCodeModelType", qrCodeModelType);
        Validator.isRequired("data", data);
        Validator.isBetween("unitWidth", unitWidth, 1, 32);
    }

    public Orientation getOrientation() {
        return orientation;
    }

    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public QRCodeModelType getQrCodeModelType() {
        return qrCodeModelType;
    }

    public void setQrCodeModelType(QRCodeModelType qrCodeModelType) {
        this.qrCodeModelType = qrCodeModelType;
    }

    public double getUnitWidth() {
        return unitWidth;
    }

    public void setUnitWidth(double unitWidth) {
        this.unitWidth = unitWidth;
    }

    public QRCodeDataInterface getData() {
        return data;
    }

    public void setData(QRCodeDataInterface data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "QRCode [orientation=" + orientation + ", position=" + position + ", qrCodeModelType=" + qrCodeModelType + ", unitWidth=" + unitWidth
                + ", data=" + data + "]";
    }
}
