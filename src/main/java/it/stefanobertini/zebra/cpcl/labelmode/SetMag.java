package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.Validator;

public class SetMag extends AbstractCommand implements LabelModeCommandInterface {

    private int width;
    private int height;

    public SetMag() {
        this(1, 1);
    }

    public SetMag(int width, int height) {
        super();
        this.width = width;
        this.height = height;
    }

    public String getCommand() {
        return "SETMAG";
    }

    @Override
    protected void getCommandLineInternal() {
        appendText(getCommand());
        appendText(" ");
        appendText(width);
        appendText(" ");
        appendText(height);
        endLine();
    }

    @Override
    public void validate() {
        Validator.isMoreThanOrEqualTo("width", width, 0);
        Validator.isMoreThanOrEqualTo("height", height, 0);
    }

    /**
     * @return the width
     */
    public int getWidth() {
        return width;
    }

    /**
     * @param width
     *            the width to set
     */
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * @return the height
     */
    public int getHeight() {
        return height;
    }

    /**
     * @param height
     *            the height to set
     */
    public void setHeight(int height) {
        this.height = height;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SetMag [width=" + width + ", height=" + height + "]";
    }

}
