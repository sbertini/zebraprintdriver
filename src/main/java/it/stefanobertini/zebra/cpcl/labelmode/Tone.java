package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractIntegerParameterCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.Validator;

public class Tone extends AbstractIntegerParameterCommand implements LabelModeCommandInterface {

    public Tone() {
        this(0);
    }

    public Tone(int tone) {
        setParameter(tone);
    }

    public String getCommand() {
        return "TONE";
    }

    @Override
    public void validate() {
        Validator.isBetween("tone", getParameter(), -99, 200);
    }

    public int getTone() {
        return getParameter();
    }

    public void setTone(int tone) {
        setParameter(tone);
    }

    @Override
    public String toString() {
        return "Tone [tone=" + getParameter() + "]";
    }

}
