package it.stefanobertini.zebra.cpcl.labelmode;

import it.stefanobertini.zebra.AbstractNoParameterCommand;
import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.Validator;
import it.stefanobertini.zebra.enums.UnitsType;

public class Units extends AbstractNoParameterCommand implements LabelModeCommandInterface {

    private UnitsType unitsType;

    public Units() {
        this(UnitsType.dots);
    }

    public Units(UnitsType unitsType) {
        this.unitsType = unitsType;
    }

    public String getCommand() {
        return unitsType.getCode();
    }

    @Override
    public void validate() {
        Validator.isRequired("unitsType", unitsType);
    }

    /**
     * @return the unitsType
     */
    public UnitsType getUnitsType() {
        return unitsType;
    }

    /**
     * @param unitsType
     *            the unitsType to set
     */
    public void setUnitsType(UnitsType unitsType) {
        this.unitsType = unitsType;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Units [unitsType=" + unitsType + "]";
    }

}
