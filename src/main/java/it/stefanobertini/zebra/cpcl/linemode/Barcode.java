package it.stefanobertini.zebra.cpcl.linemode;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.LineModeCommandInterface;
import it.stefanobertini.zebra.Validator;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.enums.BarcodeRatio;
import it.stefanobertini.zebra.enums.BarcodeType;

public class Barcode extends AbstractCommand implements LineModeCommandInterface {

    private String barcodeType;
    private double width;
    private String ratio;
    private double height;
    private Position position = new Position(0, 0);
    private String data;

    public Barcode(BarcodeType barcodeType, BarcodeRatio ratio, double width, double height, String data) {
        this(barcodeType.getCode(), ratio.getCode(), width, height, data);
    }

    public Barcode(String barcodeType, String ratio, double width, double height, String data) {
        super();
        this.barcodeType = barcodeType;
        this.width = width;
        this.ratio = ratio;
        this.height = height;
        this.data = data;
    }

    public String getCommand() {
        return "! U1 BARCODE";
    }

    @Override
    public void getCommandLineInternal() {

        appendText(getCommand());

        appendText(" ");
        appendText(barcodeType);
        appendText(" ");
        appendText(width);
        appendText(" ");
        appendText(ratio);
        appendText(" ");
        appendText(height);
        appendText(" ");
        appendText(position.getCommandLine());
        appendText(" ");
        appendText(data);

        endLine();
    }

    @Override
    public void validate() {
        Validator.isRequired("barcodeType", barcodeType);
        Validator.isRequired("position", position);
        Validator.isRequired("ratio", ratio);
        Validator.isRequired("data", data);
        Validator.isMoreThanOrEqualTo("width", width, 0);
        Validator.isMoreThanOrEqualTo("height", height, 0);
    }

    /**
     * @return the barcodeType
     */
    public String getBarcodeType() {
        return barcodeType;
    }

    /**
     * @param barcodeType
     *            the barcodeType to set
     */
    public void setBarcodeType(String barcodeType) {
        this.barcodeType = barcodeType;
    }

    /**
     * @return the width
     */
    public double getWidth() {
        return width;
    }

    /**
     * @param width
     *            the width to set
     */
    public void setWidth(double width) {
        this.width = width;
    }

    /**
     * @return the ratio
     */
    public String getRatio() {
        return ratio;
    }

    /**
     * @param ratio
     *            the ratio to set
     */
    public void setRatio(String ratio) {
        this.ratio = ratio;
    }

    /**
     * @return the height
     */
    public double getHeight() {
        return height;
    }

    /**
     * @param height
     *            the height to set
     */
    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "Barcode [barcodeType=" + barcodeType + ", width=" + width + ", ratio=" + ratio + ", height=" + height + ", position=" + position + ", data="
                + data + "]";
    }

}
