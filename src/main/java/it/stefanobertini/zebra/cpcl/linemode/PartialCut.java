package it.stefanobertini.zebra.cpcl.linemode;

import it.stefanobertini.zebra.AbstractNoParameterCommand;
import it.stefanobertini.zebra.LineModeCommandInterface;

public class PartialCut extends AbstractNoParameterCommand implements LineModeCommandInterface {

    public String getCommand() {
        return "! U1 PARTIAL-CUT";
    }

    @Override
    public void validate() {
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "PartialCut []";
    }

}
