package it.stefanobertini.zebra.cpcl.linemode;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.LineModeCommandInterface;
import it.stefanobertini.zebra.Validator;

public class SetFormFeed extends AbstractCommand implements LineModeCommandInterface {

    private double maxFeed;
    private double skipLength;

    public SetFormFeed() {
        this(0, 0);
    }

    public SetFormFeed(double maxFeed, double skipLength) {
        super();
        this.maxFeed = maxFeed;
        this.skipLength = skipLength;
    }

    public String getCommand() {
        return "SETFF";
    }

    @Override
    protected void getCommandLineInternal() {
        appendText("! U");
        endLine();
        appendText(getCommand());
        appendText(" ");
        appendText(maxFeed);
        appendText(" ");
        appendText(skipLength);
        endLine();
        appendText("PRINT");
        endLine();
    }

    @Override
    public void validate() {
        Validator.isMoreThanOrEqualTo("maxFeed", maxFeed, 0);
        Validator.isMoreThanOrEqualTo("skipLength", skipLength, 0);
    }

    public double getMaxFeed() {
        return maxFeed;
    }

    public void setMaxFeed(double maxFeed) {
        this.maxFeed = maxFeed;
    }

    public double getSkipLength() {
        return skipLength;
    }

    public void setSkipLength(double skipLength) {
        this.skipLength = skipLength;
    }

    @Override
    public String toString() {
        return "SetFormFeed [maxFeed=" + maxFeed + ", skipLength=" + skipLength + "]";
    }

}
