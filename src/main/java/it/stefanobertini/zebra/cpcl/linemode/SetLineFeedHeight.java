package it.stefanobertini.zebra.cpcl.linemode;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.LineModeCommandInterface;
import it.stefanobertini.zebra.Validator;

public class SetLineFeedHeight extends AbstractCommand implements LineModeCommandInterface {

    private double height;

    public SetLineFeedHeight() {
        this(0);
    }

    public SetLineFeedHeight(double height) {
        this.height = height;
    }

    public String getCommand() {
        return "! U1 SETLF";
    }

    @Override
    protected void getCommandLineInternal() {
        appendText(getCommand());
        appendText(" ");
        appendText(height);
        endLine();
    }

    @Override
    public void validate() {
        Validator.isMoreThanOrEqualTo("height", height, 0);
    }

    /**
     * @return the height
     */
    public double getHeight() {
        return height;
    }

    /**
     * @param height
     *            the height to set
     */
    public void setHeight(double height) {
        this.height = height;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SetLineFeedHeight [height=" + height + "]";
    }

}
