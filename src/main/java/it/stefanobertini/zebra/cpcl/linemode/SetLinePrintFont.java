package it.stefanobertini.zebra.cpcl.linemode;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.LineModeCommandInterface;
import it.stefanobertini.zebra.Validator;
import it.stefanobertini.zebra.beans.Font;

public class SetLinePrintFont extends AbstractCommand implements LineModeCommandInterface {

    private Font font;
    private double height;

    public SetLinePrintFont() {
        this(null, 0);
    }

    public SetLinePrintFont(Font font, double height) {
        super();
        this.font = font;
        this.height = height;
    }

    public String getCommand() {
        return "! U1 SETLP";
    }

    @Override
    protected void getCommandLineInternal() {
        appendText(getCommand());
        appendText(" ");
        appendText(font.getCommandLine());
        appendText(" ");
        appendText(height);
        endLine();
    }

    @Override
    public void validate() {
        Validator.isMoreThanOrEqualTo("height", height, 0);
        Validator.isRequired("font", font);
    }

    /**
     * @return the font
     */
    public Font getFont() {
        return font;
    }

    /**
     * @param font
     *            the font to set
     */
    public void setFont(Font font) {
        this.font = font;
    }

    /**
     * @return the height
     */
    public double getHeight() {
        return height;
    }

    /**
     * @param height
     *            the height to set
     */
    public void setHeight(double height) {
        this.height = height;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SetLinePrintFont [font=" + font + ", height=" + height + "]";
    }

}
