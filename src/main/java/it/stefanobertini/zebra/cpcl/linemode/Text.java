package it.stefanobertini.zebra.cpcl.linemode;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.LineModeCommandInterface;

import java.util.ArrayList;
import java.util.List;

public class Text extends AbstractCommand implements LineModeCommandInterface {

    private List<String> data;

    public Text() {
        data = new ArrayList<String>();
    }

    public Text(String text, boolean addNewLine) {
        this();

        data.add(text + (addNewLine ? "\r\n" : ""));
    }

    public Text(String text) {
        this(text, true);
    }

    public Text(List<String> text, boolean addNewLine) {
        this();

        for (String item : text) {
            if (addNewLine) {
                data.add(item + (item.endsWith("\r\n") ? "" : "\r\n"));
            } else {
                data.add(item);
            }
        }
    }

    public Text(List<String> text) {
        this(text, true);
    }

    public String getCommand() {
        return "";
    }

    @Override
    protected void getCommandLineInternal() {
        for (String item : data) {
            appendText(item);
        }

    }

    @Override
    public void validate() {
    }

    public List<String> getData() {
        return data;
    }

    public void setData(List<String> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Text [data=" + data + "]";
    }

}
