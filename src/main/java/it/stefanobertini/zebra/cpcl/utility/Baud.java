package it.stefanobertini.zebra.cpcl.utility;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.CommandInterface;
import it.stefanobertini.zebra.Validator;
import it.stefanobertini.zebra.enums.BaudRate;

public class Baud extends AbstractCommand implements CommandInterface {

    private String baudRate;

    public Baud() {
        this(BaudRate.bauds_115200);
    }

    public Baud(BaudRate baudRate) {
        this(baudRate.getCode());
    }

    public Baud(String baudRate) {
        this.baudRate = baudRate;
    }

    @Override
    public String getCommand() {
        return "BAUD";
    }

    @Override
    public void getCommandLineInternal() {
        appendText("! UTILITIES");
        endLine();
        appendText(getCommand());
        appendText(" ");
        appendText(baudRate);
        endLine();
        appendText("PRINT");
        endLine();
    }

    @Override
    public void validate() {
        Validator.isRequired("baudRate", baudRate);
    }

    public String getBaudRate() {
        return baudRate;
    }

    public void setBaudRate(String baudRate) {
        this.baudRate = baudRate;
    }

    public void setBaudRate(BaudRate baudRate) {
        this.baudRate = baudRate.getCode();
    }

    @Override
    public String toString() {
        return "Baud [baudRate=" + baudRate + "]";
    }

}
