package it.stefanobertini.zebra.cpcl.utility;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.CommandInterface;

public class Dir extends AbstractCommand implements CommandInterface {

    @Override
    public String getCommand() {
        return "DIR";
    }

    @Override
    public void getCommandLineInternal() {
        appendText("! UTILITIES");
        endLine();
        appendText(getCommand());
        endLine();
        appendText("PRINT");
        endLine();
    }

    @Override
    public void validate() {
    }

    @Override
    public String toString() {
        return "Dir []";
    }
}
