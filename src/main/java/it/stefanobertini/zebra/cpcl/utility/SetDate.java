package it.stefanobertini.zebra.cpcl.utility;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.CommandInterface;
import it.stefanobertini.zebra.Validator;

import java.text.SimpleDateFormat;
import java.util.Date;

public class SetDate extends AbstractCommand implements CommandInterface {

    private Date date;

    public SetDate() {
        this(new Date());
    }

    public SetDate(Date date) {
        super();
        setDate(date);
    }

    @Override
    public String getCommand() {
        return "SET-DATE";
    }

    @Override
    public void getCommandLineInternal() {
        SimpleDateFormat format = new SimpleDateFormat("MM-dd-yyyy");
        appendText("! UTILITIES");
        endLine();
        appendText(getCommand());
        appendText(" ");
        appendText(format.format(date));
        endLine();
        appendText("PRINT");
        endLine();
    }

    @Override
    public void validate() {
        Validator.isRequired("date", date);
    }

    public Date getDate() {
        return date == null ? null : new Date(date.getTime());
    }

    public void setDate(Date date) {
        this.date = date == null ? null : new Date(date.getTime());
    }

    @Override
    public String toString() {
        return "SetDate [date=" + date + "]";
    }

}
