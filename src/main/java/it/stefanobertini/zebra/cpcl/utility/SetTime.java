package it.stefanobertini.zebra.cpcl.utility;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.CommandInterface;
import it.stefanobertini.zebra.Validator;

import java.text.SimpleDateFormat;
import java.util.Date;

public class SetTime extends AbstractCommand implements CommandInterface {

    private Date time;

    public SetTime() {
        this(new Date());
    }

    public SetTime(Date time) {
        super();
        setTime(time);
    }

    @Override
    public String getCommand() {
        return "SET-TIME";
    }

    @Override
    public void getCommandLineInternal() {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        appendText("! UTILITIES");
        endLine();
        appendText(getCommand());
        appendText(" ");
        appendText(format.format(time));
        endLine();
        appendText("PRINT");
        endLine();
    }

    @Override
    public void validate() {
        Validator.isRequired("time", time);
    }

    public Date getTime() {
        return time == null ? null : new Date(time.getTime());
    }

    public void setTime(Date time) {

        this.time = time == null ? null : new Date(time.getTime());
    }

    @Override
    public String toString() {
        return "SetTime [time=" + time + "]";
    }

}
