package it.stefanobertini.zebra.cpcl.utility;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.CommandInterface;
import it.stefanobertini.zebra.Validator;

public class TimeOut extends AbstractCommand implements CommandInterface {

    private int timeout;

    public TimeOut() {
        this(0);
    }

    public TimeOut(int timeout) {
        super();
        this.timeout = timeout;
    }

    @Override
    public String getCommand() {
        return "TIMEOUT";
    }

    @Override
    public void getCommandLineInternal() {
        appendText("! UTILITIES");
        endLine();
        appendText(getCommand());
        appendText(" ");
        appendText(timeout);
        endLine();
        appendText("PRINT");
        endLine();
    }

    @Override
    public void validate() {
        Validator.isMoreThanOrEqualTo("timeout", timeout, 0);
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    @Override
    public String toString() {
        return "TimeOut [timeout=" + timeout + "]";
    }

}
