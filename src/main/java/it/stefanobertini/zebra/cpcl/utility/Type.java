package it.stefanobertini.zebra.cpcl.utility;

import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.CommandInterface;
import it.stefanobertini.zebra.Validator;

public class Type extends AbstractCommand implements CommandInterface {

    private String fileName;

    public Type() {
        this("");
    }

    public Type(String fileName) {
        super();
        this.fileName = fileName;
    }

    @Override
    public String getCommand() {
        return "TYPE";
    }

    @Override
    public void getCommandLineInternal() {
        appendText("! UTILITIES");
        endLine();
        appendText(getCommand());
        appendText(" ");
        appendText(fileName);
        endLine();
        appendText("PRINT");
        endLine();
    }

    @Override
    public void validate() {
        Validator.isRequired("fileName", fileName);
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public String toString() {
        return "Type [fileName=" + fileName + "]";
    }
}
