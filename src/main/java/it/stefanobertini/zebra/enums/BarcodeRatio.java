package it.stefanobertini.zebra.enums;

public enum BarcodeRatio {

    RATIO_1D5_TO_1__0("0"), RATIO_2D0_TO_1__1("1"), RATIO_2D5_TO_1__2("2"), RATIO_3D0_TO_1__3("3"), RATIO_3D5_TO_1__4("4"), RATIO_2D0_TO_1__20("20"), RATIO_2D1_TO_1__21(
            "21"), RATIO_2D2_TO_1__22("22"), RATIO_2D3_TO_1__23("23"), RATIO_2D4_TO_1__24("24"), RATIO_2D5_TO_1__25("25"), RATIO_2D6_TO_1__26("26"), RATIO_2D7_TO_1__27(
            "27"), RATIO_2D8_TO_1__28("28"), RATIO_2D9_TO_1__29("29"), RATIO_3D0_TO_1__30("30");

    private final String code;

    private BarcodeRatio(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
