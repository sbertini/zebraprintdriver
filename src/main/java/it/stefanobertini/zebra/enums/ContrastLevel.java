package it.stefanobertini.zebra.enums;

public enum ContrastLevel {

    defaultContrast(0), medium(1), dark(2), veryDark(3);

    private final int code;

    private ContrastLevel(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
