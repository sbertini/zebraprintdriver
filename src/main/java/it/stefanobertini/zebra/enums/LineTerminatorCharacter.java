package it.stefanobertini.zebra.enums;

public enum LineTerminatorCharacter {

    carriageReturn("CR"), lineFeed("LF"), carriageReturnAndLineFeed("CR-LF"), carriageReturnAndAnyThenLineFeed("CR-X-LF");

    private final String code;

    private LineTerminatorCharacter(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
