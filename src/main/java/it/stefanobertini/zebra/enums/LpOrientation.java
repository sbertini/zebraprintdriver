package it.stefanobertini.zebra.enums;

public enum LpOrientation {
    horizontal("0"), vertical("270");

    private final String code;

    private LpOrientation(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
