package it.stefanobertini.zebra.enums;

public enum QRCodeErrorCorrectionLevel {

    ultraHighReliability("H"), highReliability("Q"), standard("M"), highDensity("L");

    private final String code;

    private QRCodeErrorCorrectionLevel(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
