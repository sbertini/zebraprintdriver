package it.stefanobertini.zebra.enums;

public enum TextRotation {
    horizontal("TEXT"), vertical("TEXT90"), rotate90("TEXT90"), rotate180("TEXT180"), rotate270("TEXT270");

    private final String code;

    private TextRotation(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
