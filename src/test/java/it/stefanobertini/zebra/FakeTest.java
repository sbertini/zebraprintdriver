package it.stefanobertini.zebra;

import static org.junit.Assert.assertEquals;
import it.stefanobertini.zebra.AbstractCommand;
import it.stefanobertini.zebra.FormatUtils;

import org.junit.Test;

public class FakeTest {

    @Test
    public void test1() {
	class FakeClass extends AbstractCommand {

	    @Override
	    public void validate() {
	    }

	    @Override
	    protected String getCommand() {
		return null;
	    }

	    @Override
	    protected void getCommandLineInternal() {
		appendText(null);
	    }
	}

	FakeClass fakeClass = new FakeClass();
	String result = new String(fakeClass.getCommandByteArray());

	assertEquals("", result);
    }

    @Test
    public void test2() {
	new FormatUtils();
    }
}
