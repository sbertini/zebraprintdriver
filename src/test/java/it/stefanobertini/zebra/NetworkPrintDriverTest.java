package it.stefanobertini.zebra;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import it.stefanobertini.zebra.LabelModeCommandInterface;
import it.stefanobertini.zebra.NetworkPrintDriver;
import it.stefanobertini.zebra.PrintJob;
import it.stefanobertini.zebra.cpcl.labelmode.Abort;
import it.stefanobertini.zebra.cpcl.labelmode.End;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.net.ConnectException;
import java.net.ServerSocket;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class NetworkPrintDriverTest {

    private final static int PORT = 22334;
    private final static int BAD_PORT = 22335;
    private ServerSocket serverSocket;
    private NetworkPrintDriver driver;

    @Before
    public void setUp() throws Exception {
	serverSocket = new ServerSocket(PORT);
    }

    @After
    public void tearDown() throws Exception {
	if (serverSocket != null) {
	    serverSocket.close();
	}
	if (driver != null) {
	    driver.close();
	}
    }

    @Test
    public void testConnect() throws Exception {
	driver = new NetworkPrintDriver("127.0.0.1", PORT);
	driver.open();
    }

    @Test(expected = ConnectException.class)
    public void testConnectError() throws Exception {
	driver = new NetworkPrintDriver("127.0.0.1", BAD_PORT);
	driver.open();
    }

    @Test
    public void testSendMessage() throws Exception {
	byte[] received;

	driver = new NetworkPrintDriver("127.0.0.1", PORT);
	driver.open();

	ServerSocketThread serverSocketThread = new ServerSocketThread(serverSocket);
	serverSocketThread.start();

	Abort command = new Abort();

	driver.executeNoWait(command);

	while (!serverSocketThread.isFinished()) {
	    Thread.sleep(100);
	}

	received = serverSocketThread.getReceived();

	assertArrayEquals("Command Send", command.getCommandByteArray(), received);
    }

    @Test
    public void testSendMessageDebugStream() throws Exception {

	ByteArrayOutputStream bos = new ByteArrayOutputStream();
	PrintStream debugStream = new PrintStream(bos);

	driver = new NetworkPrintDriver("127.0.0.1", PORT);
	driver.setDebugStream(debugStream);
	driver.open();

	ServerSocketThread serverSocketThread = new ServerSocketThread(serverSocket);
	serverSocketThread.start();

	Abort command = new Abort();

	driver.executeNoWait(command);

	while (!serverSocketThread.isFinished()) {
	    Thread.sleep(100);
	}

	assertArrayEquals("Debug command", "Driver: ABORT\r\n\n".getBytes(), bos.toByteArray());
    }

    @Test
    public void testReceiveOk() throws Exception {
	String expectedAnswer = "TEST-ANSWER";
	String answer;

	driver = new NetworkPrintDriver("127.0.0.1", PORT);
	driver.open();

	ServerSocketThread serverSocketThread = new ServerSocketThread(serverSocket, ("\"" + expectedAnswer + "\"").getBytes(), 500);
	serverSocketThread.start();

	Abort command = new Abort();

	answer = driver.executeAndWait(command, 1000);

	while (!serverSocketThread.isFinished()) {
	    Thread.sleep(100);
	}

	assertEquals(expectedAnswer, answer);
    }

    @Test
    public void testReceiveTimeout() throws Exception {
	String expectedAnswer = "";
	String answer;

	driver = new NetworkPrintDriver("127.0.0.1", PORT);
	driver.open();

	ServerSocketThread serverSocketThread = new ServerSocketThread(serverSocket, expectedAnswer.getBytes(), 50);
	serverSocketThread.start();

	Abort command = new Abort();

	answer = driver.executeAndWait(command, 10);

	while (!serverSocketThread.isFinished()) {
	    Thread.sleep(100);
	}

	assertEquals(expectedAnswer, answer);
    }

    @Test
    public void testReceiveOkCustomTimeout() throws Exception {
	String expectedAnswer = "TEST-ANSWER";
	String answer;

	driver = new NetworkPrintDriver("127.0.0.1", PORT);
	driver.setDefaultAnswerWaitMillis(600);

	driver.open();

	ServerSocketThread serverSocketThread = new ServerSocketThread(serverSocket, expectedAnswer.getBytes(), 500);
	serverSocketThread.start();

	Abort command = new Abort();

	answer = driver.executeAndWait(command);

	while (!serverSocketThread.isFinished()) {
	    Thread.sleep(100);
	}

	assertEquals(expectedAnswer, answer);
    }

    @Test
    public void testExecute() throws Exception {

	PrintJob<LabelModeCommandInterface> printJob = new PrintJob<LabelModeCommandInterface>();

	printJob.add(new Abort());
	printJob.add(new End());

	byte[] expected;

	expected = "ABORT\r\nEND\r\n".getBytes();

	driver = new NetworkPrintDriver("127.0.0.1", PORT);
	driver.setDefaultAnswerWaitMillis(600);

	driver.open();

	ServerSocketThread serverSocketThread = new ServerSocketThread(serverSocket);
	serverSocketThread.start();

	driver.execute(printJob);

	while (!serverSocketThread.isFinished()) {
	    Thread.sleep(100);
	}

	assertArrayEquals(expected, serverSocketThread.getReceived());

    }
}
