package it.stefanobertini.zebra.beans;

import it.stefanobertini.zebra.beans.Font;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.beans.QRCodeManualDataItem;
import it.stefanobertini.zebra.beans.ScalableConcatBitmapText;
import it.stefanobertini.zebra.beans.ScalableConcatScalableText;
import it.stefanobertini.zebra.beans.Size;

import org.junit.Test;

public class FakeTest {

    @Test
    public void testFont() {
	Font font = new Font("4", 0);
	font.getFont();
	font.getSize();
	font.toString();
    }

    @Test
    public void testPosition() {
	Position position = new Position();
	position.toString();
    }

    @Test
    public void testQRCodeManualDataItem() {
	QRCodeManualDataItem item = new QRCodeManualDataItem(null, null);
	item.setCharacterMode(null);
	item.setData(null);
	item.toString();
    }

    @Test
    public void testScalableConcatBitmapText() {
	ScalableConcatBitmapText item = new ScalableConcatBitmapText(null, 0, null);
	item.setFont(null);
	item.setOffset(0);
	item.setText("");
	item.toString();
    }

    @Test
    public void testScalableConcatScalableText() {
	ScalableConcatScalableText item = new ScalableConcatScalableText(null, 0, 0, 0, null);
	item.toString();
    }

    @Test
    public void testSize() {
	Size item = new Size(0, 0);
	item.setHeight(0);
	item.setWidth(0);
	item.getHeight();
	item.getWidth();
	item.toString();
    }
}
