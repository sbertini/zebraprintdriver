package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandInterface;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.Aztec;
import it.stefanobertini.zebra.enums.Orientation;

import org.junit.Test;

public class AztecTest {

    @Test
    public void test1() {

	CommandInterface command = new Aztec(Orientation.horizontal, new Position(50, 100), 7, 47, "123456789012");

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("BARCODE AZTEC 50 100 XD 7 EC 47");
	output.printLn("123456789012");
	output.printLn("ENDAZTEC");

	assertCommand(output, command);
    }

    @Test
    public void test2() {

	CommandInterface command = new Aztec(Orientation.vertical, new Position(10, 20), 17, 67, "123456789012");

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("VBARCODE AZTEC 10 20 XD 17 EC 67");
	output.printLn("123456789012");
	output.printLn("ENDAZTEC");

	assertCommand(output, command);
    }

    public void fakeTest() {
	Aztec command = new Aztec();
	command.setData("");
	command.setErrorCorrection(0);
	command.setOrientation(Orientation.horizontal);
	command.setPosition(null);
	command.setUnitWidth(0);
	command.getData();
	command.getErrorCorrection();
	command.getOrientation();
	command.getPosition();
	command.getUnitWidth();
	command.toString();
    }
}
