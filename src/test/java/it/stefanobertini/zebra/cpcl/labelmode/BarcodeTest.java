package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.beans.Font;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.Barcode;
import it.stefanobertini.zebra.enums.BarcodeRatio;
import it.stefanobertini.zebra.enums.BarcodeType;
import it.stefanobertini.zebra.enums.Orientation;

import org.junit.Test;

public class BarcodeTest {

    @Test
    public void test1() {

	Barcode command = new Barcode(BarcodeType.Code128, BarcodeRatio.RATIO_2D0_TO_1__1, 1, 50, new Position(150, 10), "HORIZ.");

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("BARCODE 128 1 1 50 150 10 HORIZ.");

	assertCommand(output, command);
    }

    @Test
    public void test2() {

	Barcode command = new Barcode(Orientation.vertical, BarcodeType.Code128, BarcodeRatio.RATIO_1D5_TO_1__0, 2, 150, new Position(110, 210), "VERT.");

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("VBARCODE 128 2 0 150 110 210 VERT.");

	assertCommand(output, command);
    }

    @Test
    public void test3() {

	Barcode command = new Barcode(BarcodeType.Code128, BarcodeRatio.RATIO_2D0_TO_1__1, 1, 50, new Position(150, 10), "HORIZ.", new Font("4", 0), 10);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("BARCODE-TEXT 4 0 10");
	output.printLn("BARCODE 128 1 1 50 150 10 HORIZ.");
	output.printLn("BARCODE-TEXT OFF");

	assertCommand(output, command);
    }
}
