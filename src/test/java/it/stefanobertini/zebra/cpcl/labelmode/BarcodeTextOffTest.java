package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.cpcl.labelmode.BarcodeTextOff;

import org.junit.Test;

public class BarcodeTextOffTest {

    @Test
    public void test() {

	BarcodeTextOff command = new BarcodeTextOff();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("BARCODE-TEXT OFF");

	assertCommand(output, command);
    }
}
