package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.Box;

import org.junit.Test;

public class BoxTest {

    @Test
    public void test1() {

	Box command = new Box(new Position(1, 2), new Position(3, 4));

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("BOX 1 2 3 4 1");

	assertCommand(output, command);
    }

    @Test
    public void test2() {

	Box command = new Box(new Position(1, 2), new Position(3, 4), 5);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("BOX 1 2 3 4 5");

	assertCommand(output, command);
    }

    @Test
    public void test3() {

	Box command = new Box(new Position(1.1, 2.2), new Position(3.3, 4.4), 5.5);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("BOX 1.1 2.2 3.3 4.4 5.5");

	assertCommand(output, command);
    }

    @Test
    public void test4() {

	Box command = new Box(new Position(.1, .2), new Position(.3, .4), .5);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("BOX 0.1 0.2 0.3 0.4 0.5");

	assertCommand(output, command);
    }
}
