package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.beans.Font;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.Concat;
import it.stefanobertini.zebra.enums.Orientation;

import org.junit.Test;

public class ConcatTest {

    @Test
    public void test() {
	Concat command = new Concat(Orientation.horizontal, new Position(1, 2));
	command.addText(new Font("3", 4), 5, "First line");
	command.addText(new Font("7", 8), 9, "Second line");

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("CONCAT 1 2");
	output.printLn("3 4 5 First line");
	output.printLn("7 8 9 Second line");
	output.printLn("ENDCONCAT");

	assertCommand(output, command);
    }

}
