package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.cpcl.labelmode.Count;

import org.junit.Test;

public class CountTest {

    @Test
    public void test() {
	Count command = new Count(10);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("COUNT 10");

	assertCommand(output, command);
    }

    @Test
    public void testDefault() {
	Count command = new Count();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("COUNT 0");

	assertCommand(output, command);
    }

}
