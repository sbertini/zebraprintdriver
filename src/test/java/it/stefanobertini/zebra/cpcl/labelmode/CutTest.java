package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.cpcl.labelmode.Cut;

import org.junit.Test;

public class CutTest {

    @Test
    public void test() {
	Cut command = new Cut();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("CUT");

	assertCommand(output, command);
    }

}
