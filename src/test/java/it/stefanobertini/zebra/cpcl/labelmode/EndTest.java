package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.cpcl.labelmode.End;

import org.junit.Test;

public class EndTest {

    @Test
    public void test() {
	End command = new End();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("END");

	assertCommand(output, command);
    }

}
