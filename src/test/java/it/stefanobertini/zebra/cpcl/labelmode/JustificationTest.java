package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.cpcl.labelmode.Justification;
import it.stefanobertini.zebra.enums.JustificationType;

import org.junit.Test;

public class JustificationTest {

    @Test
    public void testLeft() {
	Justification command = new Justification(JustificationType.left);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("LEFT");

	assertCommand(output, command);
    }

    @Test
    public void testCenter() {
	Justification command = new Justification(JustificationType.center);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("CENTER");

	assertCommand(output, command);
    }

    @Test
    public void testRight() {
	Justification command = new Justification(JustificationType.right);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("RIGHT");

	assertCommand(output, command);
    }
}
