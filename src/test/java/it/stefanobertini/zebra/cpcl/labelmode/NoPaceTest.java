package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.cpcl.labelmode.NoPace;

import org.junit.Test;

public class NoPaceTest {

    @Test
    public void test() {
	NoPace command = new NoPace();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("NO-PACE");

	assertCommand(output, command);
    }

}
