package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.cpcl.labelmode.Pace;

import org.junit.Test;

public class PaceTest {

    @Test
    public void test() {
	Pace command = new Pace();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("PACE");

	assertCommand(output, command);
    }

}
