package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.labelmode.PageWidth;

import org.junit.Test;

public class PageWidthTest {

    @Test
    public void test() {
	PageWidth command = new PageWidth();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("PAGE-WIDTH 0");

	assertCommand(output, command);
    }

    @Test
    public void testDefault() {
	PageWidth command = new PageWidth(10);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("PAGE-WIDTH 10");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation() {
	PageWidth command = new PageWidth(-1);

	command.getCommandByteArray();
    }
}
