package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.cpcl.labelmode.Print;

import org.junit.Test;

public class PrintTest {

    @Test
    public void test() {
	Print command = new Print();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("PRINT");

	assertCommand(output, command);
    }

}
