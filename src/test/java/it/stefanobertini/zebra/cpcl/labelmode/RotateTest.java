package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.labelmode.Rotate;

import org.junit.Test;

public class RotateTest {

    @Test
    public void test() {
	Rotate command = new Rotate(45);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("ROTATE 45");

	assertCommand(output, command);
    }

    @Test
    public void testDefault() {
	Rotate command = new Rotate();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("ROTATE 0");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidate1() {
	Rotate command = new Rotate(-1);

	command.getCommandByteArray();
    }

    @Test(expected = ValidationException.class)
    public void testValidate2() {
	Rotate command = new Rotate(361);

	command.getCommandByteArray();
    }

}
