package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.cpcl.labelmode.SetMag;

import org.junit.Test;

public class SetMagTest {

    @Test
    public void test() {
	SetMag command = new SetMag(1, 2);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("SETMAG 1 2");

	assertCommand(output, command);
    }

    @Test
    public void testDefault() {
	SetMag command = new SetMag();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("SETMAG 1 1");

	assertCommand(output, command);
    }

}
