package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.beans.Font;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.labelmode.Text;
import it.stefanobertini.zebra.enums.TextRotation;

import org.junit.Test;

public class TextTest {

    @Test
    public void test() {
	Text command = new Text(TextRotation.horizontal, new Font("4", 0), new Position(200, 100), "TEXT");

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("TEXT 4 0 200 100 TEXT");

	assertCommand(output, command);
    }

}
