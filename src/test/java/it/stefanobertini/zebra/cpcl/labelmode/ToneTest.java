package it.stefanobertini.zebra.cpcl.labelmode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.labelmode.Tone;

import org.junit.Test;

public class ToneTest {

    @Test
    public void testDefault() {
	Tone command = new Tone();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("TONE 0");

	assertCommand(output, command);
    }

    @Test
    public void test() {
	Tone command = new Tone(1);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("TONE 1");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation1() {
	Tone command = new Tone(-100);

	command.getCommandByteArray();
    }

    @Test(expected = ValidationException.class)
    public void testValidation2() {
	Tone command = new Tone(201);

	command.getCommandByteArray();
    }
}
