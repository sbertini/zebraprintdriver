package it.stefanobertini.zebra.cpcl.linemode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.linemode.CutAt;

import org.junit.Test;

public class CutAtTest {

    @Test
    public void test() {
	CutAt command = new CutAt(10);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! U1 CUT-AT 10");

	assertCommand(output, command);
    }

    @Test
    public void testDefault() {
	CutAt command = new CutAt();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! U1 CUT-AT");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation() {
	CutAt command = new CutAt(-1);

	command.getCommandByteArray();
    }
}
