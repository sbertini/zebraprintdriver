package it.stefanobertini.zebra.cpcl.linemode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.beans.Position;
import it.stefanobertini.zebra.cpcl.linemode.Move;
import it.stefanobertini.zebra.enums.Orientation;

import org.junit.Test;

public class MoveTest {

    @Test
    public void testX0() {
        Move command = new Move(new Position(1, 0));

        CommandOutputBuilder output = new CommandOutputBuilder();
        output.printLn("! U1 XY 1 0");

        assertCommand(output, command);
    }

    @Test
    public void testY0() {
        Move command = new Move(new Position(0, 1));

        CommandOutputBuilder output = new CommandOutputBuilder();
        output.printLn("! U1 XY 0 1");

        assertCommand(output, command);
    }

    public void testXY() {
        Move command = new Move(new Position(1, 2));

        CommandOutputBuilder output = new CommandOutputBuilder();
        output.printLn("! U1 XY 1 2");

        assertCommand(output, command);
    }

    @Test
    public void testRelX0() {
        Move command = new Move(new Position(1, 0), true);

        CommandOutputBuilder output = new CommandOutputBuilder();
        output.printLn("! U1 RXY 1 0");

        assertCommand(output, command);
    }

    @Test
    public void testRelY0() {
        Move command = new Move(new Position(0, 1), true);

        CommandOutputBuilder output = new CommandOutputBuilder();
        output.printLn("! U1 RXY 0 1");

        assertCommand(output, command);
    }

    @Test
    public void testRelXY() {
        Move command = new Move(new Position(1, 2), true);

        CommandOutputBuilder output = new CommandOutputBuilder();
        output.printLn("! U1 RXY 1 2");

        assertCommand(output, command);
    }

    @Test
    public void testX() {
        Move command = new Move(1, Orientation.horizontal, false);

        CommandOutputBuilder output = new CommandOutputBuilder();
        output.printLn("! U1 X 1");

        assertCommand(output, command);
    }

    @Test
    public void testY() {
        Move command = new Move(1, Orientation.vertical, false);

        CommandOutputBuilder output = new CommandOutputBuilder();
        output.printLn("! U1 Y 1");

        assertCommand(output, command);
    }

    @Test
    public void testRelX() {
        Move command = new Move(1, Orientation.horizontal, true);

        CommandOutputBuilder output = new CommandOutputBuilder();
        output.printLn("! U1 RX 1");

        assertCommand(output, command);
    }

    @Test
    public void testRelY() {
        Move command = new Move(1, Orientation.vertical, true);

        CommandOutputBuilder output = new CommandOutputBuilder();
        output.printLn("! U1 RY 1");

        assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testBadY() {
        Move command = new Move(new Position(0, -1));

        command.getCommandByteArray();
    }
}
