package it.stefanobertini.zebra.cpcl.linemode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.linemode.PageHeight;

import org.junit.Test;

public class PageHeightTest {

    @Test
    public void test() {
	PageHeight command = new PageHeight(10);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! U1 PH 10");

	assertCommand(output, command);
    }

    @Test
    public void testDefault() {
	PageHeight command = new PageHeight();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! U1 PH 0");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation() {
	PageHeight command = new PageHeight(-1);

	command.getCommandByteArray();
    }
}
