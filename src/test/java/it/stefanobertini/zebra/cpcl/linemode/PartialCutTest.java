package it.stefanobertini.zebra.cpcl.linemode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.cpcl.linemode.PartialCut;

import org.junit.Test;

public class PartialCutTest {

    @Test
    public void test() {
	PartialCut command = new PartialCut();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! U1 PARTIAL-CUT");

	assertCommand(output, command);
    }

}
