package it.stefanobertini.zebra.cpcl.linemode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.linemode.PresentAt;

import org.junit.Test;

public class PresentAtTest {

    @Test
    public void test() {
	PresentAt command = new PresentAt(10);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! U1 PRESENT-AT 10");

	assertCommand(output, command);
    }

    @Test
    public void testDefault() {
	PresentAt command = new PresentAt();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! U1 PRESENT-AT");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation() {
	PresentAt command = new PresentAt(-1);

	command.getCommandByteArray();
    }

}
