package it.stefanobertini.zebra.cpcl.linemode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.linemode.SetLineFeedHeight;

import org.junit.Test;

public class SetLineFeedHeightTest {

    @Test
    public void test() {
	SetLineFeedHeight command = new SetLineFeedHeight(1);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! U1 SETLF 1");

	assertCommand(output, command);
    }

    @Test
    public void testDefault() {
	SetLineFeedHeight command = new SetLineFeedHeight();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! U1 SETLF 0");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation1() {
	SetLineFeedHeight command = new SetLineFeedHeight(-1);

	command.getCommandByteArray();
    }
}
