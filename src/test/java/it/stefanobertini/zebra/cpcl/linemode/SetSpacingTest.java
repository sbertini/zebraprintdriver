package it.stefanobertini.zebra.cpcl.linemode;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.linemode.SetSpacing;

import org.junit.Test;

public class SetSpacingTest {

    @Test
    public void test() {
	SetSpacing command = new SetSpacing(10);

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! U1 SETSP 10");

	assertCommand(output, command);
    }

    @Test
    public void testDefault() {
	SetSpacing command = new SetSpacing();

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! U1 SETSP 0");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation() {
	SetSpacing command = new SetSpacing(-1);

	command.getCommandByteArray();
    }
}
