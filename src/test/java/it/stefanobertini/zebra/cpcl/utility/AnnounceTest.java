package it.stefanobertini.zebra.cpcl.utility;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.utility.Announce;

import org.junit.Test;

public class AnnounceTest {

    @Test
    public void test() {
	Announce command = new Announce("TEST");

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! UTILITIES");
	output.printLn("ANNOUNCE TEST");
	output.printLn("PRINT");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation() {
	Announce command = new Announce();

	command.getCommandByteArray();
    }

    @Test
    public void testFake() {
	Announce command = new Announce("TEST");
	command.setMessage("");
	command.getMessage();
	command.toString();
    }
}
