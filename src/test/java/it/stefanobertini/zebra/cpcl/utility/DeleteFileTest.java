package it.stefanobertini.zebra.cpcl.utility;

import static it.stefanobertini.zebra.CommandAssert.assertCommand;
import it.stefanobertini.zebra.CommandOutputBuilder;
import it.stefanobertini.zebra.ValidationException;
import it.stefanobertini.zebra.cpcl.utility.DeleteFile;

import org.junit.Test;

public class DeleteFileTest {

    @Test
    public void test() {
	DeleteFile command = new DeleteFile("TEST.TXT");

	CommandOutputBuilder output = new CommandOutputBuilder();
	output.printLn("! UTILITIES");
	output.printLn("DEL TEST.TXT");
	output.printLn("PRINT");

	assertCommand(output, command);
    }

    @Test(expected = ValidationException.class)
    public void testValidation() {
	DeleteFile command = new DeleteFile();

	command.getCommandByteArray();
    }

    @Test
    public void testFake() {
	DeleteFile command = new DeleteFile();
	command.setFileName("");
	command.getFileName();
	command.toString();
    }
}
